// Copyright (C) 2019 Himanshu Kapoor

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Gravefire.Utils {
  using static CultureInfo;

  public struct Rect {
    public double X, Y, Width, Height;

    public Rect(double x, double y, double width, double height) {
      X = x;
      Y = y;
      Width = width;
      Height = height;
    }

    public static Rect BetweenPoints(double x1, double y1, double x2, double y2) {
      var minX = Math.Min(x1, x2);
      var minY = Math.Min(y1, y2);
      var maxX = Math.Max(x1, x2);
      var maxY = Math.Max(y1, y2);

      return new Rect(minX, minY, maxX - minX, maxY - minY);
    }

    public static Rect BetweenPoints(Point a, Point b) => BetweenPoints(a.X, a.Y, b.X, b.Y);

    public void IteratePerimeter(Action<Point> fn)  => IteratePerimeter((x, y) => fn(new Point(x, y)));

    public void IteratePerimeter(Action<double, double> fn) {
      for (var i = X; i <= X + Width; i++) {
        fn(i, Y);
        fn(i, Y + Height);
      }

      for (var i = Y + 1; i <= Y + Height - 1; i++) {
        fn(X, i);
        fn(X + Width, i);
      }
    }

    public Rect Union(params Rect[] rects) {
      if (rects.Length == 0) return this;
      if (rects.Length > 1) {
        return Union(rects[0])
          .Union(rects.Skip(1).ToArray());
      }

      var rect = rects[0];

      var x1 = Math.Min(X, rect.X);
      var x2 = Math.Max(X + Width, rect.X + rect.Width);
      var y1 = Math.Min(Y, rect.Y);
      var y2 = Math.Max(Y + Height, rect.Y + rect.Height);

      return new Rect(x1, y1, x2 - x1, y2 - y1);
    }

    public Rect Intersection(params Rect[] rects) {
      if (rects.Length == 0) return this;
      if (rects.Length > 1) {
        return Intersection(rects[0])
          .Intersection(rects.Skip(1).ToArray());
      }

      var rect = rects[0];

      var x1 = Math.Max(X, rect.X);
      var x2 = Math.Min(X + Width, rect.X + rect.Width);
      var y1 = Math.Max(Y, rect.Y);
      var y2 = Math.Min(Y + Height, rect.Y + rect.Height);

      if (x1 > x2 || y1 > y2) return new Rect(0, 0, 0, 0);

      return new Rect(x1, y1, x2 - x1, y2 - y1);
    }

    public List<Point> InnerPoints {
      get {
        var pts = new List<Point>();
        for (var i = X; i < X + Width; i++)
          for (var j = Y; j < Y + Height; j++)
            pts.Add(new Point(i, j));

        return pts;
      }
    }

    public Rect Pad(double padding) {
      return new Rect(X - padding, Y - padding, Width + padding * 2, Height + padding * 2);
    }

    public override bool Equals(object obj) {
      return Equals(obj as Rect?);
    }

    public bool Equals(Rect? p) {
      if (Object.ReferenceEquals(p, null))
        return false;

      if (Object.ReferenceEquals(this, p))
        return true;

      if (GetType() != p.GetType())
        return false;

      Rect pt = p ?? new Rect(double.MaxValue, double.MaxValue, double.MaxValue, double.MaxValue);
      return ToString() == p.ToString();
    }

    public override int GetHashCode() {
      unchecked {
        int hash = 17;

        hash = hash * 23 + X.GetHashCode();
        hash = hash * 23 + Y.GetHashCode();
        hash = hash * 23 + Width.GetHashCode();
        hash = hash * 23 + Height.GetHashCode();

        return hash;
      }
    }

    public static bool operator ==(Rect a, Rect b) => a.Equals(b);

    public static bool operator !=(Rect a, Rect b) => !a.Equals(b);

    public override string ToString() {
      return $"X={X}, Y={Y}, Width={Width}, Height={Height}";
    }
  }

  [TypeConverter(typeof(PointConverter))]
  public struct Point : IComparable, IEquatable<Point?> {
    public double X { get; set; }
    public double Y { get; set; }

    public Point(double x, double y) {
      X = x;
      Y = y;
    }

    public static Point FromString(string str) {
      Point pt;
      try {
        str = str.ToUpper();
        var regex = new Regex(@"X=(?<X>[0-9\-\.]+), ?Y=(?<Y>[0-9\-\.]+)");
        var match = regex.Match(str);
        var x = double.Parse(match.Groups["X"].Value, InvariantCulture);
        var y = double.Parse(match.Groups["Y"].Value, InvariantCulture);
        pt = new Point(x, y);
      } catch {
        throw new ArgumentException("Malformed String");
      }

      return pt;
    }

    public static List<Point> List(params double[] xys) {
      if (xys.Length % 2 != 0)
        throw new ArgumentException("Odd number of arguments cannot be sent to create a list of points.");

      var list = new List<Point>();
      for (var i = 0; i < xys.Length; i++) {
        list.Add(new Point(xys[i], xys[++i]));
      }
      return list;
    }

    public int CompareTo(object obj)  {
      if (!(obj is Point) || obj == null)
        return -10;
      var pt = (Point) obj;
      var val = X.CompareTo(pt.X);
      if (val == 0)
        return Y.CompareTo(pt.Y);
      return val;
    }

    public override bool Equals(object obj) {
      return Equals(obj as Point?);
    }

    public bool Equals(Point? p) {
      if (Object.ReferenceEquals(p, null))
        return false;

      if (Object.ReferenceEquals(this, p))
        return true;

      if (GetType() != p.GetType())
        return false;

      Point pt = p ?? new Point(double.MaxValue, double.MaxValue);
      return ToString() == p.ToString();
    }

    public override int GetHashCode() {
      unchecked {
        int hash = 17;
        // Suitable nullity checks etc, of course :)
        hash = hash * 23 + X.GetHashCode();
        hash = hash * 23 + Y.GetHashCode();
        return hash;
      }
    }

    public override string ToString() {
      return $"X={X}, Y={Y}";
    }

    public static bool operator ==(Point a, Point b) => a.Equals(b);

    public static bool operator !=(Point a, Point b) => !a.Equals(b);

    public Point Translate(Point delta) {
      return new Point(X + delta.X, Y + delta.Y);
    }

    public Point Translate(double dx, double dy) {
      return new Point(X + dx, Y + dy);
    }
  }

  public class PointConverter : TypeConverter {
    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType) {
      if (sourceType == typeof(string)) return true;
      return base.CanConvertFrom(context, sourceType);
    }

    public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value) {
      if (value is string) return Point.FromString(value as string);
      return base.ConvertFrom(context, culture, value);
    }

    public override object ConvertTo(ITypeDescriptorContext context,
      CultureInfo culture, object value, Type destinationType) {
      if (destinationType == typeof(string))
        return ((Point) value).ToString();
      return base.ConvertTo(context, culture, value, destinationType);
    }
  }
}
