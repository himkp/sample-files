<!-- // Copyright (c) 2019 Himanshu Kapoor

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->


```pg
SELECT
  (SELECT COUNT("games"."id") as "gamesPlayed" FROM games
  WHERE
    (users.id = games."userId1" OR
    users.id = games."userId2") AND
    games."endedOn" IS NOT NULL AND
    games."startedOn" IS NOT NULL
  ),
  (SELECT COUNT(games.id) as "gamesWon" FROM games
    WHERE
      (users.id = games."userId1" AND
      games.info::json->'players'->0->>'hasWon' = 'true') OR
      (users.id = games."userId2" AND
      games.info::json->'players'->1->>'hasWon' = 'true')
  ),
  (SELECT COUNT(games.id) as "gamesLost" FROM games
    WHERE
      (users.id = games."userId1" AND
      games.info::json->'players'->0->>'hasLost' = 'true') OR
      (users.id = games."userId2" AND
      games.info::json->'players'->1->>'hasLost' = 'true')
  ),
  (SELECT SUM(games."endedOn" - games."startedOn") * 1000 AS "totalPlayTime" FROM games
    WHERE
    (users.id = games."userId1" OR
    users.id = games."userId2") AND
    games."endedOn" IS NOT NULL AND
    games."startedOn" IS NOT NULL AND
    games.info::json->'players'->0->>'hasDisconnected' != 'true' AND
    games.info::json->'players'->1->>'hasDisconnected' != 'true'
  ),
  "displayName",
  "info"::json->>'platform' as "platform",
  "experience"
FROM users
WHERE "isBot" = false 
ORDER BY "experience" DESC, "elo" DESC
LIMIT 100
```
