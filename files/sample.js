// Copyright (c) 2019 Himanshu Kapoor

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

window.fl = fl || {};

fl.uniqueId = (function () {
	var i = 0;
	return function () {
		return ++i;
	};
})();

fl.spriteContainsPoint = function (sp, pt) {
	pt = sp.convertToNodeSpace(pt);
	var size = sp.getContentSize();
	var boundingBox = cc.rect(0, 0, size.width, size.height);

    if (typeof sp.opacity === 'number') {
        return sp.visible && sp.opacity > 0 && cc.rectContainsPoint(boundingBox, pt);
    } else {
        return sp.visible && cc.rectContainsPoint(boundingBox, pt);
    }
};

fl.translatePoint = function (point, relativeToCorner) {
	relativeToCorner = relativeToCorner || 'top-left';

	var winSize = cc.winSize;
	var originPt, originSignPt;

	switch (relativeToCorner) {
		case 'bottom-left':
			originPt = cc.p(0, 0);
			originSignPt = cc.p(1, 1);
			break;
		case 'bottom-right':
			originPt = cc.p(winSize.width, 0);
			originSignPt = cc.p(-1, 1);
			break;
		case 'top-left':
			originPt = cc.p(0, winSize.height);
			originSignPt = cc.p(1, -1);
			break;
		case 'top-right':
			originPt = cc.p(winSize.width, winSize.height);
			originSignPt = cc.p(-1, -1);
			break;
	}
	return cc.pAdd(originPt, cc.pCompMult(originSignPt, point));
};

fl.pRatio = function (v1, v2, ratio) {
	return cc.pAdd(v1, cc.pMult(cc.pSub(v2, v1), ratio));
};

fl.p = function (x, y, width, height) {
	var size = cc.winSize, designSize = fl.designResolution;
	return cc.p(size.width / 2 - designSize.width / 2 + x / 2 + width / 4, size.height - y / 2 - height / 4);
};

fl.noop = function () {};

fl.rand = function (min, max) {
	return Math.floor(fl.randf(min, max + 1));
};

fl.randf = function (min, max) {
	return Math.random() * (max - min) + min;
};

fl.computed = function (obj, prop) {
	if (arguments.length > 2) {
		for (var i = 1, l = arguments.length; i < l; i++) {
			fl.computed(obj, arguments[i]);
		}
		return;
	}

	var propUpper = prop.charAt(0).toUpperCase() + prop.substr(1);
	cc.defineGetterSetter(obj, prop, obj['get' + propUpper], obj['set' + propUpper]);
};

fl.audio = function (src) {
	var elem = document.createElement('audio');
	elem.src = src;
	elem.load();
	return elem;
};

fl.tween = function (obj, prop, init, fin, duration, cb) {
	var t = 0, dt = 1 / 60;
	obj[prop] = init;
	var i = setInterval(function () {
		t = Math.min(t + dt, duration);
		obj[prop] = init + (fin - init) * t / duration;
		if (t === duration) {
			clearInterval(i);
			if (typeof cb === 'function') {
				cb();
			}
		}
	}, dt * 1000);
};

fl.uint8ArrayToString = function (u8a){
	var CHUNK_SZ = 0x8000;
	var c = [];
	for (var i=0; i < u8a.length; i += CHUNK_SZ) {
		c.push(String.fromCharCode.apply(null, u8a.subarray(i, i + CHUNK_SZ)));
	}
	return c.join('');
};

fl.uint32ArrayToString = function (u32a) {
	return fl.uint8ArrayToString(fl.uint32ArrayToUint8Array(u32a));
};

fl.uint8ToHex = function (num) {
	var string = num.toString(16);
	for (var i = string.length; i < 2; i++) {
		string = '0' + string;
	}
	return string;
};

fl.uint32toHex = function (num) {
	var string = num.toString(16);
	for (var i = string.length; i < 8; i++) {
		string = '0' + string;
	}
	return string;
};

fl.uint32ArrayToUint8Array = function (u32a) {
	var a = new Uint8Array(u32a.length * 4);
	var str = '', i;
	for (i = 0; i < u32a.length; i++) {
		str += fl.uint32toHex(u32a[i]);
	}
	for (i = 0; i < str.length; i += 2) {
		a[i / 2] = parseInt(str.substr(i, 2), 16);
	}
	return a;
};

fl.uint8ArrayToUint32Array = function (u8a) {
	var a = new Uint32Array(u8a.length / 4);
	var str = '', i;
	for (i = 0; i <= u8a.length; i++) {
		if (i && i % 4 === 0) {
			a[i / 4 - 1] = parseInt(str, 16);
			str = '';
			if (i === u8a.length) {
				break;
			}
		}
		str += fl.uint8ToHex(u8a[i]);
	}
	return a;
};

fl.YES = 'yes';
fl.NO = 'no';
fl.MAYBE ='maybe';

fl.forEach = function (obj, fn, ctx) {
	if (obj === null || typeof obj === 'undefined') {
		return;
	}
	if (obj instanceof Array) {
		for (var i = 0; i < obj.length; i++) {
			if (fn.call(ctx, obj[i], i, obj)) {
				break;
			}
		}
		return;
	}
	var keys = Object.keys(obj), key;
	while ((key = keys.shift())) {
		if (fn.call(ctx, obj[key], key, obj)) {
			break;
		}
	}
};

fl.weightedRandom = function (list, weights) {
	var i, totalWeight = 0;
	for (i = weights.length; i--;) {
		totalWeight += weights[i];
	}

	var randomNum = fl.randf(0, totalWeight);
	var weightSum = 0;

	for (i = 0; i < list.length; i++) {
		weightSum += weights[i];
		weightSum = +weightSum.toFixed(2);

		if (randomNum <= weightSum) {
			return list[i];
		}
	}
};

fl.pToString = function (pt) {
	return pt.x + ',' + pt.y;
};

String.prototype.scramble = function (level) {
	// level = 1 to 3, 1 = easy, 2 = medium, 3 = hard
	level = level || 2;

	var str = this.toString();
	var l = Math.floor((str.length - level / 3 * str.length) / 2);
	for (var i = 0; i < str.length; i++) {
		var pos = fl.rand(l, str.length - 1 - l);
		var pos2 = fl.rand(l, str.length - 2 - l);
		var ch = str.charAt(pos);
		var before = str.substring(0, pos);
		var after = str.substring(pos + 1);
		var temp = before + after;
		before = temp.substring(0, pos2);
		after = temp.substring(pos2);
		str = before + ch + after;
	}
	if (str === this.toString()) {
		return str.scramble();
	}
	return str;
};
