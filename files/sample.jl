# Copyright (c) 2017, Júlio Hoffimann <julio.hoffimann@gmail.com>

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


"""
    Process
A geological process of evolution.
"""
abstract type Process end

"""
    evolve!(state, proc, Δt)
Evolve the `state` with process `proc` for a time period `Δt`.
"""
evolve!(state, proc, Δt::Float64) = error("not implemented")

"""
    TimelessProcess
A geological process implemented without the notion of time.
"""
abstract type TimelessProcess <: Process end

"""
    evolve!(land, proc)
Evolve the `land` with timeless process `proc`.
"""
evolve!(land::Matrix, proc::TimelessProcess) = error("not implemented")

"""
    evolve!(land, proc, Δt)
Evolve the `land` with timeless process `proc` for a time period `Δt`.
"""
function evolve!(land::Matrix, proc::TimelessProcess, Δt::Float64)
  t = mean(land)
  evolve!(land, proc)
  @. land = t + Δt + land
  nothing
end

"""
    evolve!(state, proc, Δt)
Evolve the landscape `state` with timeless process `proc` for a time period `Δt`.
"""
evolve!(state::LandState, proc::TimelessProcess, Δt::Float64) =
  evolve!(state.land, proc, Δt)

#------------------
# IMPLEMENTATIONS
#------------------
include("processes/geostats.jl")
include("processes/smoothing.jl")
include("processes/sequential.jl")
include("processes/analytical.jl")
