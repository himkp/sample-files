// Copyright (c) 2019 Himanshu Kapoor

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.fleonlabs.sheepingaround;

import android.app.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PlayGamesAuthProvider;

import java.util.Date;

public class FirebaseAuthHelper {
  private static final String WEBCLIENT_ID = "994366844753-kjvdrebt5kdga9oim1tq22vastfmigmr.apps.googleusercontent.com";
  private static final String TAG = "FirebaseAuthHelper";
  public static final int RC_GOOGLE_SIGN_IN = 90081;

  private static FirebaseAuth auth;
  private static Activity activity;

  private static GetTokenResult authToken;
  private static boolean isAnonymous = false;
  public static GoogleSignInAccount googleAccount;

  public static void init(Activity act) {
    activity = act;
    auth = FirebaseAuth.getInstance();
  }

  public static void updateAuthToken() {
    try {
      FirebaseUser user = auth.getCurrentUser();
      user.getIdToken(true).addOnCompleteListener(activity, task -> {
        if (task.isSuccessful()) {
          authToken = task.getResult();
        } else {
          Log.d(TAG, "Error: error fetching token");
          // try again
          updateAuthToken();
        }
      });
    } catch (NullPointerException e) {
      Log.d(TAG, "Error: " + e.getMessage());
    }
  }

  private static boolean isSignedIn() {
    GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
    if (account != null) {
      googleAccount = account;
    }
    return false;
  }

  public static void signIn() {
    try {
      auth.getCurrentUser();
    } catch (NullPointerException e) {
      return;
    } finally {
      activity.runOnUiThread(() -> {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestServerAuthCode(WEBCLIENT_ID)
                .requestId()
                .build();

        GoogleSignInClient signInClient = GoogleSignIn.getClient(activity, gso);
        Intent intent = signInClient.getSignInIntent();
        activity.startActivityForResult(intent, RC_GOOGLE_SIGN_IN);
      });
    }
  }

  public static void setGoogleAccount(GoogleSignInAccount googleAccount) {
    FirebaseAuthHelper.googleAccount = googleAccount;
    activity.runOnUiThread(() -> {
      firebaseSignIn(googleAccount.getServerAuthCode());
    });
  }

  @NonNull
  public static String getAuthToken() {
    String token = "";
    try {
      token = authToken.getToken();
      long expirationTimestamp = authToken.getExpirationTimestamp();
      long now = new Date().getTime() / 1000;
      if (expirationTimestamp < now) {
        updateAuthToken();
        return "";
      }
    } catch (NullPointerException e) {
    } finally {
      if (isAnonymous)
        return "anonymous";
      return token;
    }
  }

  public static void firebaseSignIn(String serverAuthCode) {
    AuthCredential credential = PlayGamesAuthProvider.getCredential(serverAuthCode);
    auth.signInWithCredential(credential)
      .addOnCompleteListener(activity, task -> {
        if (task.isSuccessful()) {
          // Sign in success, update UI with the signed-in user's information
          Log.d(TAG, "signInWithCredential:success");
          updateAuthToken();
        } else {
          // try again if sign in fails
          firebaseSignIn(serverAuthCode);
        }
      });
  }

  public static void signInAnonymously() {
    isAnonymous = true;
    Log.d(TAG, "Reverting to anonymous login");
  }
}
