# https://github.com/carnaval/jopml/blob/master/snake.mips

# t7 -> Black, s7 -> blue
li $ t7,7
li $ s7, 2
	#initialization: coloring of the edges (therefore from the inside in white)
li $ a1, 1
li $ a2, 1
init:
	muli $ a3, $ a2, 16
	add $ a3, $ a3, $ a1  
	sw $ t7, 0 ($ a3)
	addi $ a1, $ a1, 1
	suffered $ t5, $ a1, 15
	beqz $ t5, init2
	i init
	init2:
		li $ a1, 1
		addi $ a2, $ a2, 1
		suffered $ t5, $ a2, 15
		beqz $ t5, main
		i init
	# a1, (a2): current position
	# t0, t1, t2, t3: keyHandlers on pressed de kc = 38 (Up), 40 (Down), 37 (Left), 39 (Right)
	# v0: random (0..255)
	# t7: 7 = (111) -> White color, zero = (000) -> Black color
	# s1: current direction (Up: -16, Down: +16, Left: -1, Right: +1) / $ s2: direction sign (0: +, 1: -)
	# k0: tail length -1 (k0 = 0 -> tail = 1)
	# t4: Score
li $ t4, 0
hand:
	li $ k0, 3
	li $ a1, 132
	sw $ a1, 1036 ($ zero)
	li $ a1, 131
	sw $ a1, 1032 ($ zero)
	li $ a1, 130
	sw $ a1, 1028 ($ zero)
	li $ a1, 129
	sw $ a1, 1024 ($ zero)
	li $ s1, 1
	li $ s2, 0
	i genecherry
mainb:
	lw $ t8, 0 ($ a1)
	beqz $ t8, gameover
	suffered $ t5, $ t8, 2
	beqz $ t5, gotcherry
INAC:
	sw $ zero, 0 ($ a1)
gestionqueue:
		# We erase the first block (the old one)
	lw $ t9, 1024 ($ k0)
	sw $ t7, 0 ($ t9)
		# We switch the entire tail
	move $ k1, $ k0
switchqueue:
		# When k1 = 0, we stop
	beqz $ k1, finswitch
		# We load the next reg and store it in the current one
	lw $ s6, 1020 ($ k1)
	sw $ s6, 1024 ($ k1)
		# And we increment $ k1
	suffered $ k1, $ k1, 1
	j switchqueue
finswitch:
		# We store the new block at the end of the queue
	sw $ a1, 1024 ($ zero)
	
	suffered $ t5, $ t0, 1
	beqz $ t5, high
	suffered $ t5, $ t1, 1
	beqz $ t5, low
	suffered $ t5, $ t2, 1
	beqz $ t5, left
	suffered $ t5, $ t3, 1
	beqz $ t5, right
makemove:
	beqz $ s2, adds
	sub $ a1, $ a1, $ s1
	j mainb
	adds:
		add $ a1, $ a1, $ s1
		j mainb
high:
	suffered $ t6, $ s1, 16
	beqz $ t6, makemove
	li $ s1, 16
	li $ s2, 1
	j makemove
low:
	suffered $ t6, $ s1, 16
	beqz $ t6, makemove
	li $ s1, 16
	li $ s2, 0
	j makemove
left:
	suffered $ t6, $ s1, 1
	beqz $ t6, makemove
	li $ s1, 1
	li $ s2, 1
	j makemove
right:
	suffered $ t6, $ s1, 1
	beqz $ t6, makemove
	li $ s1, 1
	li $ s2, 0
	j makemove
gotcherry:
	addi $ t4, $ t4, 1
	addi $ k0, $ k0, 1
	li $ t9, 10000
	sw $ t9, 1024 ($ k0)
genecherry:
		# Generate a number
	move $ v1, $ v0
		# Check if it's not in the snake or in a wall
	lw $ t5, 0 ($ v1)
	beqz $ t5, genecherry
	sw $ s7, 0 ($ v1)
	m mainc
gameover:
	li $ t7, 1
	sw $ t7, 0 ($ a1)
gameoverloop:
	sw $ t7, 0 ($ v0)
	i gameoverloop
