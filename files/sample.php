<?php
// Copyright (c) 2012 Vladimir Keleshev, <vladimir@keleshev.com>
//                    Blake Williams, <code@shabbyrobe.org>

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in 
// the Software without restriction, including without limitation the rights to 
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
// of the Software, and to permit persons to whom the Software is furnished to do 
// so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all 
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
// SOFTWARE.

if (php_sapi_name() != 'cli') {
    throw new \Exception();
}

$basePath = __DIR__;
$testPath = __DIR__.'/test';
require $basePath.'/vendor/autoload.php';

require $testPath.'/lib/TestCase.php';
require $testPath.'/lib/LanguageAgnosticTest.php';
require $testPath.'/lib/PythonPortedTest.php';

$options = array(
    'filter'        => null,
    'coverage-html' => null,
);
$options = array_merge(
    $options,
    getopt('', array('filter:', 'coverage-html:'))
);

$pyTestFile = $basePath.'/py/testcases.docopt';
if (!file_exists($pyTestFile)) {
    die("Please ensure you have loaded the git submodules\n");
}

$suite = new PHPUnit_Framework_TestSuite();
$suite->addTest(new PHPUnit_Framework_TestSuite('Docopt\Test\PythonPortedTest'));
$suite->addTest(Docopt\Test\LanguageAgnosticTest::createSuite($pyTestFile));
$suite->addTest(Docopt\Test\LanguageAgnosticTest::createSuite("$basePath/test/extra.docopt"));

$args = array(
    'filter'=>$options['filter'],
    'strict'=>true,
    'processIsolation'=>false,
    'backupGlobals'=>false,
    'backupStaticAttributes'=>false,
    'convertErrorsToExceptions'=>true,
    'convertNoticesToExceptions'=>true,
    'convertWarningsToExceptions'=>true,
    'addUncoveredFilesFromWhitelist'=>true,
    'processUncoveredFilesFromWhitelist'=>true,
);
if ($options['coverage-html']) {
    $args['coverageHtml'] = $options['coverage-html'];
}

$runner = new PHPUnit_TextUI_TestRunner();
$runner->doRun($suite, $args);
