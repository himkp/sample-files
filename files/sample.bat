REM GNU GENERAL PUBLIC LICENSE
REM                        Version 2, June 1991

REM  Copyright (C) 1989, 1991 Free Software Foundation, Inc., <http://fsf.org/>
REM  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
REM  Everyone is permitted to copy and distribute verbatim copies
REM  of this license document, but changing it is not allowed.

REM                             Preamble

REM   The licenses for most software are designed to take away your
REM freedom to share and change it.  By contrast, the GNU General Public
REM License is intended to guarantee your freedom to share and change free
REM software--to make sure the software is free for all its users.  This
REM General Public License applies to most of the Free Software
REM Foundation's software and to any other program whose authors commit to
REM using it.  (Some other Free Software Foundation software is covered by
REM the GNU Lesser General Public License instead.)  You can apply it to
REM your programs, too.

REM   When we speak of free software, we are referring to freedom, not
REM price.  Our General Public Licenses are designed to make sure that you
REM have the freedom to distribute copies of free software (and charge for
REM this service if you wish), that you receive source code or can get it
REM if you want it, that you can change the software or use pieces of it
REM in new free programs; and that you know you can do these things.

REM   To protect your rights, we need to make restrictions that forbid
REM anyone to deny you these rights or to ask you to surrender the rights.
REM These restrictions translate to certain responsibilities for you if you
REM distribute copies of the software, or if you modify it.

REM   For example, if you distribute copies of such a program, whether
REM gratis or for a fee, you must give the recipients all the rights that
REM you have.  You must make sure that they, too, receive or can get the
REM source code.  And you must show them these terms so they know their
REM rights.

REM   We protect your rights with two steps: (1) copyright the software, and
REM (2) offer you this license which gives you legal permission to copy,
REM distribute and/or modify the software.

REM   Also, for each author's protection and ours, we want to make certain
REM that everyone understands that there is no warranty for this free
REM software.  If the software is modified by someone else and passed on, we
REM want its recipients to know that what they have is not the original, so
REM that any problems introduced by others will not reflect on the original
REM authors' reputations.

REM   Finally, any free program is threatened constantly by software
REM patents.  We wish to avoid the danger that redistributors of a free
REM program will individually obtain patent licenses, in effect making the
REM program proprietary.  To prevent this, we have made it clear that any
REM patent must be licensed for everyone's free use or not licensed at all.

REM   The precise terms and conditions for copying, distribution and
REM modification follow.

REM                     GNU GENERAL PUBLIC LICENSE
REM    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

REM   0. This License applies to any program or other work which contains
REM a notice placed by the copyright holder saying it may be distributed
REM under the terms of this General Public License.  The "Program", below,
REM refers to any such program or work, and a "work based on the Program"
REM means either the Program or any derivative work under copyright law:
REM that is to say, a work containing the Program or a portion of it,
REM either verbatim or with modifications and/or translated into another
REM language.  (Hereinafter, translation is included without limitation in
REM the term "modification".)  Each licensee is addressed as "you".

REM Activities other than copying, distribution and modification are not
REM covered by this License; they are outside its scope.  The act of
REM running the Program is not restricted, and the output from the Program
REM is covered only if its contents constitute a work based on the
REM Program (independent of having been made by running the Program).
REM Whether that is true depends on what the Program does.

REM   1. You may copy and distribute verbatim copies of the Program's
REM source code as you receive it, in any medium, provided that you
REM conspicuously and appropriately publish on each copy an appropriate
REM copyright notice and disclaimer of warranty; keep intact all the
REM notices that refer to this License and to the absence of any warranty;
REM and give any other recipients of the Program a copy of this License
REM along with the Program.

REM You may charge a fee for the physical act of transferring a copy, and
REM you may at your option offer warranty protection in exchange for a fee.

REM   2. You may modify your copy or copies of the Program or any portion
REM of it, thus forming a work based on the Program, and copy and
REM distribute such modifications or work under the terms of Section 1
REM above, provided that you also meet all of these conditions:

REM     a) You must cause the modified files to carry prominent notices
REM     stating that you changed the files and the date of any change.

REM     b) You must cause any work that you distribute or publish, that in
REM     whole or in part contains or is derived from the Program or any
REM     part thereof, to be licensed as a whole at no charge to all third
REM     parties under the terms of this License.

REM     c) If the modified program normally reads commands interactively
REM     when run, you must cause it, when started running for such
REM     interactive use in the most ordinary way, to print or display an
REM     announcement including an appropriate copyright notice and a
REM     notice that there is no warranty (or else, saying that you provide
REM     a warranty) and that users may redistribute the program under
REM     these conditions, and telling the user how to view a copy of this
REM     License.  (Exception: if the Program itself is interactive but
REM     does not normally print such an announcement, your work based on
REM     the Program is not required to print an announcement.)

REM These requirements apply to the modified work as a whole.  If
REM identifiable sections of that work are not derived from the Program,
REM and can be reasonably considered independent and separate works in
REM themselves, then this License, and its terms, do not apply to those
REM sections when you distribute them as separate works.  But when you
REM distribute the same sections as part of a whole which is a work based
REM on the Program, the distribution of the whole must be on the terms of
REM this License, whose permissions for other licensees extend to the
REM entire whole, and thus to each and every part regardless of who wrote it.

REM Thus, it is not the intent of this section to claim rights or contest
REM your rights to work written entirely by you; rather, the intent is to
REM exercise the right to control the distribution of derivative or
REM collective works based on the Program.

REM In addition, mere aggregation of another work not based on the Program
REM with the Program (or with a work based on the Program) on a volume of
REM a storage or distribution medium does not bring the other work under
REM the scope of this License.

REM   3. You may copy and distribute the Program (or a work based on it,
REM under Section 2) in object code or executable form under the terms of
REM Sections 1 and 2 above provided that you also do one of the following:

REM     a) Accompany it with the complete corresponding machine-readable
REM     source code, which must be distributed under the terms of Sections
REM     1 and 2 above on a medium customarily used for software interchange; or,

REM     b) Accompany it with a written offer, valid for at least three
REM     years, to give any third party, for a charge no more than your
REM     cost of physically performing source distribution, a complete
REM     machine-readable copy of the corresponding source code, to be
REM     distributed under the terms of Sections 1 and 2 above on a medium
REM     customarily used for software interchange; or,

REM     c) Accompany it with the information you received as to the offer
REM     to distribute corresponding source code.  (This alternative is
REM     allowed only for noncommercial distribution and only if you
REM     received the program in object code or executable form with such
REM     an offer, in accord with Subsection b above.)

REM The source code for a work means the preferred form of the work for
REM making modifications to it.  For an executable work, complete source
REM code means all the source code for all modules it contains, plus any
REM associated interface definition files, plus the scripts used to
REM control compilation and installation of the executable.  However, as a
REM special exception, the source code distributed need not include
REM anything that is normally distributed (in either source or binary
REM form) with the major components (compiler, kernel, and so on) of the
REM operating system on which the executable runs, unless that component
REM itself accompanies the executable.

REM If distribution of executable or object code is made by offering
REM access to copy from a designated place, then offering equivalent
REM access to copy the source code from the same place counts as
REM distribution of the source code, even though third parties are not
REM compelled to copy the source along with the object code.

REM   4. You may not copy, modify, sublicense, or distribute the Program
REM except as expressly provided under this License.  Any attempt
REM otherwise to copy, modify, sublicense or distribute the Program is
REM void, and will automatically terminate your rights under this License.
REM However, parties who have received copies, or rights, from you under
REM this License will not have their licenses terminated so long as such
REM parties remain in full compliance.

REM   5. You are not required to accept this License, since you have not
REM signed it.  However, nothing else grants you permission to modify or
REM distribute the Program or its derivative works.  These actions are
REM prohibited by law if you do not accept this License.  Therefore, by
REM modifying or distributing the Program (or any work based on the
REM Program), you indicate your acceptance of this License to do so, and
REM all its terms and conditions for copying, distributing or modifying
REM the Program or works based on it.

REM   6. Each time you redistribute the Program (or any work based on the
REM Program), the recipient automatically receives a license from the
REM original licensor to copy, distribute or modify the Program subject to
REM these terms and conditions.  You may not impose any further
REM restrictions on the recipients' exercise of the rights granted herein.
REM You are not responsible for enforcing compliance by third parties to
REM this License.

REM   7. If, as a consequence of a court judgment or allegation of patent
REM infringement or for any other reason (not limited to patent issues),
REM conditions are imposed on you (whether by court order, agreement or
REM otherwise) that contradict the conditions of this License, they do not
REM excuse you from the conditions of this License.  If you cannot
REM distribute so as to satisfy simultaneously your obligations under this
REM License and any other pertinent obligations, then as a consequence you
REM may not distribute the Program at all.  For example, if a patent
REM license would not permit royalty-free redistribution of the Program by
REM all those who receive copies directly or indirectly through you, then
REM the only way you could satisfy both it and this License would be to
REM refrain entirely from distribution of the Program.

REM If any portion of this section is held invalid or unenforceable under
REM any particular circumstance, the balance of the section is intended to
REM apply and the section as a whole is intended to apply in other
REM circumstances.

REM It is not the purpose of this section to induce you to infringe any
REM patents or other property right claims or to contest validity of any
REM such claims; this section has the sole purpose of protecting the
REM integrity of the free software distribution system, which is
REM implemented by public license practices.  Many people have made
REM generous contributions to the wide range of software distributed
REM through that system in reliance on consistent application of that
REM system; it is up to the author/donor to decide if he or she is willing
REM to distribute software through any other system and a licensee cannot
REM impose that choice.

REM This section is intended to make thoroughly clear what is believed to
REM be a consequence of the rest of this License.

REM   8. If the distribution and/or use of the Program is restricted in
REM certain countries either by patents or by copyrighted interfaces, the
REM original copyright holder who places the Program under this License
REM may add an explicit geographical distribution limitation excluding
REM those countries, so that distribution is permitted only in or among
REM countries not thus excluded.  In such case, this License incorporates
REM the limitation as if written in the body of this License.

REM   9. The Free Software Foundation may publish revised and/or new versions
REM of the General Public License from time to time.  Such new versions will
REM be similar in spirit to the present version, but may differ in detail to
REM address new problems or concerns.

REM Each version is given a distinguishing version number.  If the Program
REM specifies a version number of this License which applies to it and "any
REM later version", you have the option of following the terms and conditions
REM either of that version or of any later version published by the Free
REM Software Foundation.  If the Program does not specify a version number of
REM this License, you may choose any version ever published by the Free Software
REM Foundation.

REM   10. If you wish to incorporate parts of the Program into other free
REM programs whose distribution conditions are different, write to the author
REM to ask for permission.  For software which is copyrighted by the Free
REM Software Foundation, write to the Free Software Foundation; we sometimes
REM make exceptions for this.  Our decision will be guided by the two goals
REM of preserving the free status of all derivatives of our free software and
REM of promoting the sharing and reuse of software generally.

REM                             NO WARRANTY

REM   11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
REM FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
REM OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
REM PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
REM OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
REM MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
REM TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
REM PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REM REPAIR OR CORRECTION.

REM   12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
REM WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REM REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
REM INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
REM OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
REM TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
REM YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
REM PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
REM POSSIBILITY OF SUCH DAMAGES.

REM                      END OF TERMS AND CONDITIONS

REM             How to Apply These Terms to Your New Programs

REM   If you develop a new program, and you want it to be of the greatest
REM possible use to the public, the best way to achieve this is to make it
REM free software which everyone can redistribute and change under these terms.

REM   To do so, attach the following notices to the program.  It is safest
REM to attach them to the start of each source file to most effectively
REM convey the exclusion of warranty; and each file should have at least
REM the "copyright" line and a pointer to where the full notice is found.

REM     {description}
REM     Copyright (C) {year}  {fullname}

REM     This program is free software; you can redistribute it and/or modify
REM     it under the terms of the GNU General Public License as published by
REM     the Free Software Foundation; either version 2 of the License, or
REM     (at your option) any later version.

REM     This program is distributed in the hope that it will be useful,
REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM     GNU General Public License for more details.

REM     You should have received a copy of the GNU General Public License along
REM     with this program; if not, write to the Free Software Foundation, Inc.,
REM     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

REM Also add information on how to contact you by electronic and paper mail.

REM If the program is interactive, make it output a short notice like this
REM when it starts in an interactive mode:

REM     Gnomovision version 69, Copyright (C) year name of author
REM     Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
REM     This is free software, and you are welcome to redistribute it
REM     under certain conditions; type `show c' for details.

REM The hypothetical commands `show w' and `show c' should show the appropriate
REM parts of the General Public License.  Of course, the commands you use may
REM be called something other than `show w' and `show c'; they could even be
REM mouse-clicks or menu items--whatever suits your program.

REM You should also get your employer (if you work as a programmer) or your
REM school, if any, to sign a "copyright disclaimer" for the program, if
REM necessary.  Here is a sample; alter the names:

REM   Yoyodyne, Inc., hereby disclaims all copyright interest in the program
REM   `Gnomovision' (which makes passes at compilers) written by James Hacker.

REM   {signature of Ty Coon}, 1 April 1989
REM   Ty Coon, President of Vice

REM This General Public License does not permit incorporating your program into
REM proprietary programs.  If your program is a subroutine library, you may
REM consider it more useful to permit linking proprietary applications with the
REM library.  If this is what you want to do, use the GNU Lesser General
REM Public License instead of this License.

@if (@X)==(@Y) @end /* Harmless hybrid line that begins a JScript comment

::************ Documentation ***********
::REPL.BAT version 3.3
:::
:::REPL  Search  Replace  [Options  [SourceVar]]
:::REPL  /?[REGEX|REPLACE]
:::REPL  /V
:::
:::  Performs a global regular expression search and replace operation on
:::  each line of input from stdin and prints the result to stdout.
:::
:::  Each parameter may be optionally enclosed by double quotes. The double
:::  quotes are not considered part of the argument. The quotes are required
:::  if the parameter contains a batch token delimiter like space, tab, comma,
:::  semicolon. The quotes should also be used if the argument contains a
:::  batch special character like &, |, etc. so that the special character
:::  does not need to be escaped with ^.
:::
:::  If called with a single argument of /?, then prints help documentation
:::  to stdout. If a single argument of /?REGEX, then opens up Microsoft's
:::  JScript regular expression documentation within your browser. If a single
:::  argument of /?REPLACE, then opens up Microsoft's JScript REPLACE
:::  documentation within your browser.
:::
:::  If called with a single argument of /V, case insensitive, then prints
:::  the version of REPL.BAT.
:::
:::  Search  - By default, this is a case sensitive JScript (ECMA) regular
:::            expression expressed as a string.
:::
:::            JScript regex syntax documentation is available at
:::            http://msdn.microsoft.com/en-us/library/ae5bf541(v=vs.80).aspx
:::
:::  Replace - By default, this is the string to be used as a replacement for
:::            each found search expression. Full support is provided for
:::            substituion patterns available to the JScript replace method.
:::
:::            For example, $& represents the portion of the source that matched
:::            the entire search pattern, $1 represents the first captured
:::            submatch, $2 the second captured submatch, etc. A $ literal
:::            can be escaped as $$.
:::
:::            An empty replacement string must be represented as "".
:::
:::            Replace substitution pattern syntax is fully documented at
:::            http://msdn.microsoft.com/en-US/library/efy6s3e6(v=vs.80).aspx
:::
:::  Options - An optional string of characters used to alter the behavior
:::            of REPL. The option characters are case insensitive, and may
:::            appear in any order.
:::
:::            I - Makes the search case-insensitive.
:::
:::            L - The Search is treated as a string literal instead of a
:::                regular expression. Also, all $ found in Replace are
:::                treated as $ literals.
:::
:::            B - The Search must match the beginning of a line.
:::                Mostly used with literal searches.
:::
:::            E - The Search must match the end of a line.
:::                Mostly used with literal searches.
:::
:::            V - Search and Replace represent the name of environment
:::                variables that contain the respective values. An undefined
:::                variable is treated as an empty string.
:::
:::            A - Only print altered lines. Unaltered lines are discarded.
:::                If both the M and V options are present, then prints the
:::                entire result if there was a change anywhere in the string.
:::                The A option is incompatible with the M option unless the S
:::                option is also present.
:::
:::            M - Multi-line mode. The entire contents of stdin is read and
:::                processed in one pass instead of line by line, thus enabling
:::                search for \n. This also enables preservation of the original
:::                line terminators. If the M option is not present, then every
:::                printed line is termiated with carriage return and line feed.
:::                The M option is incompatible with the A option unless the S
:::                option is also present.
:::
:::            X - Enables extended substitution pattern syntax with support
:::                for the following escape sequences within the Replace string:
:::
:::                \\     -  Backslash
:::                \b     -  Backspace
:::                \f     -  Formfeed
:::                \n     -  Newline
:::                \q     -  Quote
:::                \r     -  Carriage Return
:::                \t     -  Horizontal Tab
:::                \v     -  Vertical Tab
:::                \xnn   -  Extended ASCII byte code expressed as 2 hex digits
:::                \unnnn -  Unicode character expressed as 4 hex digits
:::
:::                Also enables the \q escape sequence for the Search string.
:::                The other escape sequences are already standard for a regular
:::                expression Search string.
:::
:::                Also modifies the behavior of \xnn in the Search string to work
:::                properly with extended ASCII byte codes.
:::
:::                Extended escape sequences are supported even when the L option
:::                is used. Both Search and Replace support all of the extended
:::                escape sequences if both the X and L opions are combined.
:::
:::            S - The source is read from an environment variable instead of
:::                from stdin. The name of the source environment variable is
:::                specified in the next argument after the option string. Without
:::                the M option, ^ anchors the beginning of the string, and $ the
:::                end of the string. With the M option, ^ anchors the beginning
:::                of a line, and $ the end of a line.
:::
::: REPL.BAT was written by Dave Benham, with assistance from DosTips user Aacini
::: to get \xnn to work properly with extended ASCII byte codes. REPL.BAT was
::: originally posted at: http://www.dostips.com/forum/viewtopic.php?f=3&t=3855
:::

::************ Batch portion ***********
@echo off
if .%2 equ . (
  if "%~1" equ "/?" (
    <"%~f0" cscript //E:JScript //nologo "%~f0" "^:::" "" a
    exit /b 0
  ) else if /i "%~1" equ "/?regex" (
    explorer "http://msdn.microsoft.com/en-us/library/ae5bf541(v=vs.80).aspx"
    exit /b
  ) else if /i "%~1" equ "/?replace" (
    explorer "http://msdn.microsoft.com/en-US/library/efy6s3e6(v=vs.80).aspx"
    exit /b
  ) else if /i "%~1" equ "/V" (
    <"%~f0" cscript //E:JScript //nologo "%~f0" "^::(REPL\.BAT version)" "$1" a
    exit /b
  ) else (
    call :err "Insufficient arguments"
    exit /b 1
  )
)
echo(%~3|findstr /i "[^SMILEBVXA]" >nul && (
  call :err "Invalid option(s)"
  exit /b 1
)
echo(%~3|findstr /i "M"|findstr /i "A"|findstr /vi "S" >nul && (
  call :err "Incompatible options"
  exit /b 1
)
cscript //E:JScript //nologo "%~f0" %*
exit /b 0

:err
>&2 echo ERROR: %~1. Use REPL /? to get help.
exit /b

************* JScript portion **********/
var env=WScript.CreateObject("WScript.Shell").Environment("Process");
var args=WScript.Arguments;
var search=args.Item(0);
var replace=args.Item(1);
var options="g";
if (args.length>2) options+=args.Item(2).toLowerCase();
var multi=(options.indexOf("m")>=0);
var alterations=(options.indexOf("a")>=0);
if (alterations) options=options.replace(/a/g,"");
var srcVar=(options.indexOf("s")>=0);
if (srcVar) options=options.replace(/s/g,"");
if (options.indexOf("v")>=0) {
  options=options.replace(/v/g,"");
  search=env(search);
  replace=env(replace);
}
if (options.indexOf("x")>=0) {
  options=options.replace(/x/g,"");
  replace=replace.replace(/\\\\/g,"\\B");
  replace=replace.replace(/\\q/g,"\"");
  replace=replace.replace(/\\x80/g,"\\u20AC");
  replace=replace.replace(/\\x82/g,"\\u201A");
  replace=replace.replace(/\\x83/g,"\\u0192");
  replace=replace.replace(/\\x84/g,"\\u201E");
  replace=replace.replace(/\\x85/g,"\\u2026");
  replace=replace.replace(/\\x86/g,"\\u2020");
  replace=replace.replace(/\\x87/g,"\\u2021");
  replace=replace.replace(/\\x88/g,"\\u02C6");
  replace=replace.replace(/\\x89/g,"\\u2030");
  replace=replace.replace(/\\x8[aA]/g,"\\u0160");
  replace=replace.replace(/\\x8[bB]/g,"\\u2039");
  replace=replace.replace(/\\x8[cC]/g,"\\u0152");
  replace=replace.replace(/\\x8[eE]/g,"\\u017D");
  replace=replace.replace(/\\x91/g,"\\u2018");
  replace=replace.replace(/\\x92/g,"\\u2019");
  replace=replace.replace(/\\x93/g,"\\u201C");
  replace=replace.replace(/\\x94/g,"\\u201D");
  replace=replace.replace(/\\x95/g,"\\u2022");
  replace=replace.replace(/\\x96/g,"\\u2013");
  replace=replace.replace(/\\x97/g,"\\u2014");
  replace=replace.replace(/\\x98/g,"\\u02DC");
  replace=replace.replace(/\\x99/g,"\\u2122");
  replace=replace.replace(/\\x9[aA]/g,"\\u0161");
  replace=replace.replace(/\\x9[bB]/g,"\\u203A");
  replace=replace.replace(/\\x9[cC]/g,"\\u0153");
  replace=replace.replace(/\\x9[dD]/g,"\\u009D");
  replace=replace.replace(/\\x9[eE]/g,"\\u017E");
  replace=replace.replace(/\\x9[fF]/g,"\\u0178");
  replace=replace.replace(/\\b/g,"\b");
  replace=replace.replace(/\\f/g,"\f");
  replace=replace.replace(/\\n/g,"\n");
  replace=replace.replace(/\\r/g,"\r");
  replace=replace.replace(/\\t/g,"\t");
  replace=replace.replace(/\\v/g,"\v");
  replace=replace.replace(/\\x[0-9a-fA-F]{2}|\\u[0-9a-fA-F]{4}/g,
    function($0,$1,$2){
      return String.fromCharCode(parseInt("0x"+$0.substring(2)));
    }
  );
  replace=replace.replace(/\\B/g,"\\");
  search=search.replace(/\\\\/g,"\\B");
  search=search.replace(/\\q/g,"\"");
  search=search.replace(/\\x80/g,"\\u20AC");
  search=search.replace(/\\x82/g,"\\u201A");
  search=search.replace(/\\x83/g,"\\u0192");
  search=search.replace(/\\x84/g,"\\u201E");
  search=search.replace(/\\x85/g,"\\u2026");
  search=search.replace(/\\x86/g,"\\u2020");
  search=search.replace(/\\x87/g,"\\u2021");
  search=search.replace(/\\x88/g,"\\u02C6");
  search=search.replace(/\\x89/g,"\\u2030");
  search=search.replace(/\\x8[aA]/g,"\\u0160");
  search=search.replace(/\\x8[bB]/g,"\\u2039");
  search=search.replace(/\\x8[cC]/g,"\\u0152");
  search=search.replace(/\\x8[eE]/g,"\\u017D");
  search=search.replace(/\\x91/g,"\\u2018");
  search=search.replace(/\\x92/g,"\\u2019");
  search=search.replace(/\\x93/g,"\\u201C");
  search=search.replace(/\\x94/g,"\\u201D");
  search=search.replace(/\\x95/g,"\\u2022");
  search=search.replace(/\\x96/g,"\\u2013");
  search=search.replace(/\\x97/g,"\\u2014");
  search=search.replace(/\\x98/g,"\\u02DC");
  search=search.replace(/\\x99/g,"\\u2122");
  search=search.replace(/\\x9[aA]/g,"\\u0161");
  search=search.replace(/\\x9[bB]/g,"\\u203A");
  search=search.replace(/\\x9[cC]/g,"\\u0153");
  search=search.replace(/\\x9[dD]/g,"\\u009D");
  search=search.replace(/\\x9[eE]/g,"\\u017E");
  search=search.replace(/\\x9[fF]/g,"\\u0178");
  if (options.indexOf("l")>=0) {
    search=search.replace(/\\b/g,"\b");
    search=search.replace(/\\f/g,"\f");
    search=search.replace(/\\n/g,"\n");
    search=search.replace(/\\r/g,"\r");
    search=search.replace(/\\t/g,"\t");
    search=search.replace(/\\v/g,"\v");
    search=search.replace(/\\x[0-9a-fA-F]{2}|\\u[0-9a-fA-F]{4}/g,
      function($0,$1,$2){
        return String.fromCharCode(parseInt("0x"+$0.substring(2)));
      }
    );
    search=search.replace(/\\B/g,"\\");
  } else search=search.replace(/\\B/g,"\\\\");
}
if (options.indexOf("l")>=0) {
  options=options.replace(/l/g,"");
  search=search.replace(/([.^$*+?()[{\\|])/g,"\\$1");
  replace=replace.replace(/\$/g,"$$$$");
}
if (options.indexOf("b")>=0) {
  options=options.replace(/b/g,"");
  search="^"+search
}
if (options.indexOf("e")>=0) {
  options=options.replace(/e/g,"");
  search=search+"$"
}
var search=new RegExp(search,options);
var str1, str2;

if (srcVar) {
  str1=env(args.Item(3));
  str2=str1.replace(search,replace);
  if (!alterations || str1!=str2) if (multi) {
    WScript.Stdout.Write(str2);
  } else {
    WScript.Stdout.WriteLine(str2);
  }
} else {
  while (!WScript.StdIn.AtEndOfStream) {
    if (multi) {
      WScript.Stdout.Write(WScript.StdIn.ReadAll().replace(search,replace));
    } else {
      str1=WScript.StdIn.ReadLine();
      str2=str1.replace(search,replace);
      if (!alterations || str1!=str2) WScript.Stdout.WriteLine(str2);
    }
  }
}
