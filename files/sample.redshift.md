<!-- https://docs.aws.amazon.com/mobileanalytics/latest/ug/example-redshift-queries.html -->

```redshift
SELECT
  application_app_id AS"app id",
  monetization_transaction_item_id AS"item id",
  monetization_transaction_store AS"store",
  COUNT(DISTINCTclient_id) AS"devices",
  COUNT(DISTINCTclient_cognito_id) AS"users",
  SUM(monetization_transaction_quantity) AS"quantity",
  SUM(monetization_transaction_price_amount) "amount (Apple only)",
  monetization_transaction_price_currency_code AS"currency (Apple only)"
  FROM
  AWSMA.v_event
  WHERE
  event_type = '_monetization.purchase'AND
  event_timestamp BETWEENgetdate() - 30 ANDgetdate() + 1
  GROUPBY
  "app id",
  "item id",
  "currency (Apple only)",
  "store"
  ORDERBY
  "app id"ASC,
  "item id"ASC,
  "quantity"DESC,
  "store",
  "devices"DESC
  ;
```
