// Copyright (c) 2019 Himanshu Kapoor

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

//
//  FLEventAggregator.m
//  Spellbound
//
//  Created by Fleon Games on 5/6/13.
//
//

#import "FLEventAggregator.h"
#import "FLEvent.h"
#import "Spellbound.h"

static FLEventAggregator *eventAggregator;
static NSMutableDictionary *proxyEvents;

@implementation FLProxyEvent

-(id)initWithName:(NSString *)name andDispatcherName:(NSString *)eventDispatcherName
{
    if (self = [super init])
    {
        self.name = name.stringByReplacingVariables;
        self.eventDispatcherName = eventDispatcherName.stringByReplacingVariables;
        if (!proxyEvents)
            proxyEvents = [[NSMutableDictionary alloc] init];
        if (!proxyEvents[self.eventDispatcherName])
            proxyEvents[self.eventDispatcherName] = [NSMutableDictionary dictionary];
        proxyEvents[self.eventDispatcherName][self.name] = self;
        
        id eventDispatcher = [[Spellbound eventDispatcherManager] eventDispatcherWithName:self.eventDispatcherName];
        SEL eventSelector = NSSelectorFromString([@"Event_" stringByAppendingString:self.name]);
        if (eventDispatcher && [eventDispatcher respondsToSelector:eventSelector]) {
            FLEvent *actualEvent = [eventDispatcher performSelector:eventSelector withObject:nil];
            self.eventDataComparator = actualEvent.eventDataComparator;
            actualEvent.onDispatch = ^ (id data) {
                [self dispatchWithData:data];
            };
        }
    }
    return self;
}

+(id)eventWithName:(NSString *)name andDispatcherName:(NSString *)eventDispatcherName
{
    if (proxyEvents && proxyEvents[eventDispatcherName] && proxyEvents[eventDispatcherName][name]) {
        return (FLProxyEvent *) proxyEvents[eventDispatcherName][name];
    }
    return [[[self alloc] initWithName:name andDispatcherName:eventDispatcherName] autorelease];
}

@end

@implementation FLEventDispatcherManager

- (id)init
{
    self = [super init];
    if (self) {
        eventDispatchers = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(id)eventDispatcherWithName:(NSString *)name
{
    name = name.stringByReplacingVariables;
    return eventDispatchers[name];
}

-(void)registerEventDispatcher:(id)eventDispatcher withName:(NSString *)name
{
    NSLog(@"Registered event dispatcher: %@", name);
    if (proxyEvents && proxyEvents[name])
    {
        for (FLProxyEvent *proxyEvent in [proxyEvents[name] objectEnumerator])
        {
            id oldEventDispatcher = [self eventDispatcherWithName:name];
            SEL eventSelector = NSSelectorFromString([@"Event_" stringByAppendingString:proxyEvent.name]);
            if (oldEventDispatcher && [oldEventDispatcher respondsToSelector:eventSelector])
            {
                FLEvent *oldEvent = [oldEventDispatcher performSelector:eventSelector withObject:nil];
                oldEvent.onDispatch = ^ (id data) {};
            }
            
            if ([eventDispatcher respondsToSelector:eventSelector])
            {
                FLEvent *actualEvent = [eventDispatcher performSelector:eventSelector withObject:nil];
                proxyEvent.eventDataComparator = actualEvent.eventDataComparator;
                actualEvent.onDispatch = ^ (id data) {
                    [proxyEvent dispatchWithData:data];
                };
            }
        }
    }

    eventDispatchers[name] = eventDispatcher;
}

-(void)unregisterEventDispatcher:(id)eventDispatcher
{
    NSArray *keys = [eventDispatchers allKeysForObject:eventDispatcher];
    for (NSString *key in keys)
        [eventDispatchers removeObjectForKey:key];
}

-(void)unregisterEventDispatcherWithName:(NSString *)name
{
    [eventDispatchers removeObjectForKey:name];
}

- (void)dealloc
{
    [eventDispatchers release];
    [proxyEvents release];
    [super dealloc];
}

@end

@implementation FLEventAggregator

- (id)init
{
    self = [super init];
    if (self) {
        events = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [events release];
    [super dealloc];
}

+(FLEventAggregator *)globalEventAggregator
{
    if (!eventAggregator)
    {
        eventAggregator = [[FLEventAggregator alloc] init];
    }
    return eventAggregator;
}

-(FLEvent *)eventWithName:(NSString *)eventName
{
    return events[eventName];
}

-(FLEvent *)eventWithParameterizedName:(NSString *)eventName eventData:(id *)eventData
{
    if ([eventName rangeOfString:@"("].location != NSNotFound) {
        NSRange range1 = [eventName rangeOfString:@"("];
        NSRange range2 = [eventName rangeOfString:@")"];
        NSRange range3, range4;
        NSMutableArray* eventParams = nil;
        
        if (range1.location != NSNotFound && range2.location != NSNotFound)
        {
            range3.location = range1.location + 1;
            range3.length = range2.location - range1.location - 1;
            range4.location = 0;
            range4.length = range1.location;
            
            eventParams = [[[[eventName substringWithRange:range3] componentsSeparatedByString:@","] mutableCopy] autorelease];
            for (NSUInteger i = 0; i < eventParams.count; i++)
            {
                eventParams[i] = [eventParams[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                eventParams[i] = [(NSString *)eventParams[i] stringByReplacingVariables];
            }
            eventName = [[eventName substringWithRange:range4] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
        
        
        *eventData = [NSArray arrayWithArray:eventParams];
        if (eventParams.count == 1)
        {
            *eventData = (*eventData)[0];
        }
    }
    
    NSArray *eventNameSplitOnDot = [eventName componentsSeparatedByString:@"."];
    if (eventNameSplitOnDot.count > 1) {
        NSString *eventDispatcherName = [eventNameSplitOnDot[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *eventName = [eventNameSplitOnDot[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        return [FLProxyEvent eventWithName:eventName andDispatcherName:eventDispatcherName];
    }
    return [self eventWithName:eventName];
}

-(void)addEvent:(FLEvent *)event
{
    events[event.name] = event;
}

@end
