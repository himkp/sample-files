// Copyright (c) 2019 Himanshu Kapoor

// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import { Dictionary } from 'lodash'

interface ISerializedValue {
  $type?: string

  [key: string]: any
}

interface ISerializedRef {
  $ref: number
}

type SerializedValue = NativeTypes | JSONDictionary | ISerializedValue | ISerializedRef

export interface ISerialized {
  value: SerializedValue,
  refs: JSONDictionary[]
}

export default class Serializer {
  constructor(private classMap: Dictionary<any>) { }

  serialize(obj: any, refs: JSONDictionary[] = [], map = new Map<any, number>(), $: { uid: number } = { uid: 0 }): ISerialized {
    if (obj === undefined) return { value: null, refs }
    if (typeof obj !== 'object' || obj === null) return { value: obj, refs }
    const serialized: any = Array.isArray(obj) ? [] : { }

    let id = map.get(obj)

    if (obj.constructor.name !== 'Object' && !Array.isArray(obj)) {
      if (!id) map.set(obj, id = $.uid++)

      serialized.$type = obj.constructor.name

      if (refs[id])
        return { value: { $ref: id }, refs }

      refs[id] = serialized
    }

    const keys = Object.keys(obj)
    // tslint:disable-next-line:no-conditional-assignment
    for (let key: string | undefined; key = keys.shift();) {
      serialized[key] = this.serialize(obj[key], refs, map, $).value
    }

    if (typeof id === 'undefined') {
      return { value: serialized, refs }
    }

    return { value: { $ref: id }, refs }
  }

  private isRef(obj: SerializedValue): obj is ISerializedRef {
    return !!(obj && typeof obj === 'object' && obj.hasOwnProperty('$ref'))
  }

  private isValue(obj: SerializedValue): obj is ISerializedValue {
    return !!(obj && typeof obj === 'object' && obj.hasOwnProperty('$type'))
  }

  unserialize<T>(obj: SerializedValue, refs: any[] = []): T {
    if (Array.isArray(obj))
      return obj.map((obj) => this.unserialize(obj, refs)) as any

    if (this.isRef(obj))
      obj = refs[obj.$ref]

    if (typeof obj !== 'object' || obj === null || !this.isValue(obj))
      return obj as any

    if (this.isValue(obj) && obj.$type) {
      const cls = this.classMap[obj.$type]
      if (!cls)
        throw new Error(`Unable to unserialize class "${obj.$type}".`)
      delete obj.$type

      Object.setPrototypeOf(obj, cls.prototype)
    }

    const keys = Object.keys(obj)
    // tslint:disable-next-line:no-conditional-assignment
    for (let key: string | undefined; key = keys.shift();) {
      obj[key] = this.unserialize(obj[key], refs)
    }

    return obj as any
  }

  parse(data: string) {
    const serialized = JSON.parse(data.toString())
    return this.unserialize(serialized.value, serialized.refs)
  }

  stringify(data: any, space?: string | number) {
    return JSON.stringify(this.serialize(data), null, space)
  }
}
